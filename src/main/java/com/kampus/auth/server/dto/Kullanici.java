package com.kampus.auth.server.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Kullanici {
    private String id;
    private String ad;
    private String soyad;
    private String cep;
    private String adres;
    private Long tc;
    private String email;
    private LocalDate dogumTarihi;
    private String sifre;
    private Integer cinsiyetId;
    private String veliId;
    private String okulId;
    private String bransId;
    private Integer sinifDerecesi;
    private String derslikId;
    private List<Rol> roller;
}
