package com.kampus.auth.server.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kampus.auth.server.core.dto.KullaniciPrincipalDto;
import com.kampus.auth.server.core.dto.LoginDto;
import com.kampus.auth.server.core.dto.ResponseDto;
import com.kampus.auth.server.core.enums.EnmCode;
import com.kampus.auth.server.core.helper.JsonHelper;
import com.kampus.auth.server.core.security.JwtErrorHelper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.kampus.auth.server.core.security.SecurityConstants.*;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {



    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        LoginDto creds = null;
        try {
            creds = new ObjectMapper()
                    .readValue(request.getInputStream(), LoginDto.class);

            return super.getAuthenticationManager().authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getUsername(),
                            creds.getPassword(),
                            new ArrayList<>())
            );
        } catch (Exception e) {
            if (creds == null) {
                throw new BadCredentialsException(EnmCode.AUTHENTICATION.getMessage());
            } else {
                    throw new BadCredentialsException(EnmCode.AUTHENTICATION.getMessage());
            }
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authentication) throws IOException, ServletException {
        KullaniciPrincipalDto user = ((KullaniciPrincipalDto) authentication.getPrincipal());
        user.setPassword(null);
            girisBasarili(response, user);
    }


    private void girisBasarili(HttpServletResponse response, KullaniciPrincipalDto user) throws IOException {
        responseHeader(response);

        Object token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
                .setHeaderParam("typ", TOKEN_TYPE)
                .setIssuer(TOKEN_ISSUER)
                .setAudience(TOKEN_AUDIENCE)
                .setSubject(user.getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + 864000000))
                .claim("userPrincipal", user)
                .compact();
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        String message = JwtErrorHelper.builder().build().getMessageString("success.giris");
        ResponseDto success = new ResponseDto(EnmCode.SUCCESS, message, TOKEN_PREFIX + token);
        response.getWriter().print(JsonHelper.toJson(success));
    }

    private void responseHeader(HttpServletResponse response) {
        response.setContentType("application/json");
        response.addHeader("Content-Type", "application/json");
        response.addHeader("Accept-Language", LocaleContextHolder.getLocale().getLanguage());
        response.setCharacterEncoding("UTF-8");
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        JwtErrorHelper.builder().build().getBadCredentialErrorResponse(response);
    }

}
