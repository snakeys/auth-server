package com.kampus.auth.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableEurekaClient
public class AuthServerApplication implements WebMvcConfigurer {


	public static void main(String[] args) {
		SpringApplication.run(AuthServerApplication.class, args);
	}


}
