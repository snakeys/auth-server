package com.kampus.auth.server.core.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public abstract class BaseAbstractController {

    MessageSource messageSource;

    public String getMessageFromSource(String messageKey) {
        return messageSource.getMessage(messageKey, new Object[0], LocaleContextHolder.getLocale());
    }


}
