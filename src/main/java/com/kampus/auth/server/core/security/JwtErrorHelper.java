package com.kampus.auth.server.core.security;



import com.kampus.auth.server.core.dto.ResponseDto;
import com.kampus.auth.server.core.enums.EnmCode;
import com.kampus.auth.server.core.helper.JsonHelper;
import lombok.Builder;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Builder
public class JwtErrorHelper {

    public void getBadCredentialErrorResponse(HttpServletResponse res) throws IOException {
        res.setStatus(HttpStatus.OK.value());
        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");
        res.addHeader("Content-Type", "application/json");

        String message = getMessageString("error.bad.credential");
        ResponseDto error = new ResponseDto(EnmCode.AUTHENTICATION, message, null);

        res.getWriter().print(JsonHelper.toJson(error));
    }

    public String getMessageString(String code) {
        ResourceBundleMessageSource rs = new ResourceBundleMessageSource();
        rs.setBasename("messages");
        rs.setDefaultEncoding("UTF-8");
        String message = rs.getMessage(code, new Object[0], LocaleContextHolder.getLocale());
        return message;
    }
}
