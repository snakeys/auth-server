package com.kampus.auth.server.core.security;



import com.kampus.auth.server.core.dto.KullaniciPrincipalDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public final class SecurityUtils {

    private SecurityUtils() {
    }

    public static KullaniciPrincipalDto getKullanici() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof KullaniciPrincipalDto) {
                return ((KullaniciPrincipalDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            }
        }

        return null;
    }

    public static String getKullaniciId() {
        return getKullanici() == null ? null : getKullanici().getId();
    }
}
