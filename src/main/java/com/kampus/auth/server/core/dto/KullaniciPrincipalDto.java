package com.kampus.auth.server.core.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

public class KullaniciPrincipalDto implements UserDetails {

    private final boolean accountNonExpired;
    private final boolean accountNonLocked;
    private final boolean credentialsNonExpired;
    private final boolean enabled;
    private String id;
    private String tamAdi;
    private String cep;
    private Long tc;
    private String okulId;
    private String password;
    private String username;
    private Set<GrantedAuthority> authorities;

    public KullaniciPrincipalDto() {
        accountNonExpired = true;
        accountNonLocked = true;
        credentialsNonExpired = true;
        enabled = true;
    }

    public KullaniciPrincipalDto(String id, String tamAdi, String cep,
                                 Long tc, String okulId, String password, String username,
                                 Set<GrantedAuthority> authorities) {
        this.id = id;
        this.tamAdi = tamAdi;
        this.cep = cep;
        this.tc = tc;
        this.okulId = okulId;
        this.password = password;
        this.username = username;
        this.authorities = authorities;
        accountNonExpired = true;
        accountNonLocked = true;
        credentialsNonExpired = true;
        enabled = true;
    }

    public String getId() {
        return id;
    }

    public String getTamAdi() {
        return tamAdi;
    }

    public String getCep() {
        return cep;
    }


    public Long getTc() {
        return tc;
    }

    public String getOkulId() {
        return okulId;
    }


    @Override
    @JsonDeserialize(using = CustomAuthorityDeserializer.class)
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }


}
