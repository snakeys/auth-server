package com.kampus.auth.server.core.service;


import com.kampus.auth.server.core.dto.Dto;

public interface IBaseService<T extends Dto,ID> {
    T save(T dto);

    T updateById(T dto, ID id);

    boolean deleteById(ID id);

    T findById(ID id);
}
