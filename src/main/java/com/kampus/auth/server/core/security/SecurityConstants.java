package com.kampus.auth.server.core.security;

public class SecurityConstants {
    public static final String AUTH_LOGIN_URL = "/login";

    public static final String SECRET = "GaziUniversitesi";
    public static final long EXPIRATION_TIME = 432_000_000; // 5 gün
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";

}
