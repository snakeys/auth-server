package com.kampus.auth.server.service;

import com.kampus.auth.server.core.dto.KullaniciPrincipalDto;
import com.kampus.auth.server.core.service.BaseService;
import com.kampus.auth.server.dto.Kullanici;
import com.kampus.auth.server.dto.Rol;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service(value = "userDetails")
@RequiredArgsConstructor
public class UserDetailServiceImpl extends BaseService implements UserDetailsService {

    @Autowired
    RestTemplate restTemplate;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String[] array = username.split(":");
        String finalUsername = array[0];
        Kullanici kullanici = restTemplate.getForObject("http://USER-API/api/kullanici/auth/" + finalUsername, Kullanici.class);

        KullaniciPrincipalDto kullaniciPrincipalDto = new KullaniciPrincipalDto(
                kullanici.getId(),
                kullanici.getAd() + " " + kullanici.getSoyad(),
                kullanici.getCep(),
                kullanici.getTc(),
                kullanici.getOkulId(),
                kullanici.getSifre(),
                kullanici.getId(),
                getAuthorities(kullanici)
        );
        return kullaniciPrincipalDto;
    }

    private Set<GrantedAuthority> getAuthorities(
            Kullanici kullaniciEntity) {
        List<Rol> roles = kullaniciEntity.getRoller();
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Rol rolEntity : roles) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + rolEntity.getKod()));
        }
        return new HashSet<>(authorities);
    }
}
